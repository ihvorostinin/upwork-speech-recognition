//
//  ViewController.swift
//  SpeechRecognition
//
//  Created by Ivan Khvorostinin on 12/07/2017.
//  Copyright © 2017 ikhvorostinin. All rights reserved.
//

import UIKit
import JavaScriptCore

fileprivate let webViewJSContextKeyPath = "documentView.webView.mainFrame.javaScriptContext"
fileprivate let webkitSpeechRecognitionJSType = "webkitSpeechRecognition" as NSString

class ViewController: UIViewController, UIWebViewDelegate, UITextFieldDelegate {

    static var sharedWebView: UIWebView?
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var textFieldURL: UITextField!

    private func load() {
        _ = AudioController.sharedInstance.stop()
        SpeechRecognitionService.sharedInstance.stopStreaming()

//        let htmlPath = Bundle.main.path(forResource: "Sample", ofType: "")!
//        let htmlUrl  = URL(fileURLWithPath: htmlPath).appendingPathComponent("3.htm")
        let htmlUrl  = URL(string: "https://spc-001.testing.dadasign.pl")!
        
        webView.keyboardDisplayRequiresUserAction = false;
        webView.loadRequest(URLRequest(url: htmlUrl))
        
        ViewController.sharedWebView = webView
    }
    
    func registerJS() {
        let context = webView.value(forKeyPath: webViewJSContextKeyPath) as! JSContext
        context.setObject(webkitSpeechRecognition.self, forKeyedSubscript: webkitSpeechRecognitionJSType)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        load()
    }
    
    @IBAction func textFieldURLAction(_ sender: Any) {
        load()
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        registerJS()
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        load()
        return false
    }

}

